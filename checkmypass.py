import requests  # ne ajuta sa facem interogari, este ca si cum am avea un browser dara se face fara sa avem browser
import hashlib
import sys

# prima varianta fara functie - creare  parola cu hash

# url = 'https://api.pwnedpasswords.com/range/' + 'CBFDA' # Has-am parola cu SHA1 Hash Generator - passwordsgenerator.net
# res = requests.get(url)
# print(res)

#1. Aici[facem request-ul] trimitem datele cu parola noastra la serverul API si primim un raspuns de la server, daca s-a facut corect legatura(200) sau nu (400) si am primit raspunsul

def request_api_data(query_char): # aceasta functie va avea ca request datele nostre si vom primi un raspuns
    url = 'https://api.pwnedpasswords.com/range/' + query_char
    res = requests.get(url)
    if res.status_code != 200:
        raise RuntimeError(f'Error fetching: {res.status_code}, check the API and try again')
    return res

#3. Apoi aici verificam daca parola noastra exista in has-urile primite.
# Impartim raspunsul primit de la server in doua(parola si count), apoi returnam de cate ori apare contorizat.
# daca hasul parolei noastre este gasit in baza de date a serverului il returneaza cu count cu tot; altfel returneaza 0

def get_password_leaks_count(hashes, hash_to_check):  # hashes este raspunsul de la server si hash_to check - este parola noastra pe care trebuie sa o verificam bucland prin tote hashes
    hashes = (line.split(':') for line in hashes.text.splitlines()) # atentie la methoda splitlines() daca nu o punem nu ne soliteaza cum trebuie
                                                                    # # tuple comprehension - split fiecare linie pana la : din textul parolei hasurate.
    for h, count in hashes:
        # print(h, count)  # vom avea fiecare item separat parola hasurata si contorul ei(de cate ori apare - doar pentru verificare
        if h == hash_to_check:
            return count   # de cate ori aceasta parola a fost gasita
    return 0    # daca nu returnam 0

#2. Folosind hashlib creem/afisam parola hasata:
# - scoatem in 2 variabile primele 5 caratere si urmatoarele pana la sfarsit
# - retinem din request facut la server doar primale 5 caractere intr-o variabila request
# - le returnam contorizate folosin functia anterioara

def pwned_api_check(password): # aici trebuie sa trecem parola prin SHA1 Hash Generator. Acest lucru se face la fel cu hashlib - generam Sha1password =?. apoi trebuie sa o trimitem catre API
    sha1password = hashlib.sha1(password.encode('utf-8')).hexdigest().upper()  #trebuie decodata oarola utf-8 si apoi aplata funtia hexdigest()pentru a transforma in parola hash
    first5_char, tail = sha1password[:5], sha1password[5:] # aici splitam sha1 in doua
    response = request_api_data(first5_char)  # o prindem intr-o variabila response pentru a returna doar primele 5 char din sha1 la return
    # print(first5_char, tail)  # pentru a vedea daca totul este ok
    # print(response)
    return get_password_leaks_count(response, tail)  # contorizeaza primele 5 caractere si restul

# request_api_data()  # daca aici punem o parola gresita ne da Error 400, de ex.

# 4. Facem o functie main pentru a scoate informatia returnata in terminal

def main(args):  # args reprezinta parola pe care o cautam   TODO de facut un fisier TXT cu parola si sa importam de acolo sa nu o scriem in comand line(se poate salva in computer si nu este super sigura)
    for password in args:  # pentru ca ii putem da parole multiple sa le verifice
        count = pwned_api_check(password)
        if count:   # daca count exista
            print(f'{password} was found {count} times...you should probably change your password!')
        else:
            print(f'{password} was NOT found. Carry on!')
    return 'done!'

if __name__ == '__main__': #file-ul cu aplicatia va rula doar daca este file-ul main cel pecare il rulam
    sys.exit(main(sys.argv[1:]))  # sys.exit() in cazul in care din orice motiv nu iese afara dupa apelul din terminal - va afisa done la iesirea din program